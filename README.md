## Transcriptome of weeping pinyon pine, *Pinus pinceana*, shows differences across heterogeneous habitats

Laura Figueroa-Corona, Patricia Delgado Valerio, Jill Wegrzyn, Daniel Piñero

#### Raw data avaible: 

#### This repository reproduct the bioinformatic processing: 


##### Key Message
We reconstructed the needle tissue transcriptome of *P. pinceana* for individuals from distinct biogeographic regions across a temperature and precipitation gradient that represents its natural distribution. Gene expression analysis via RNA-Seq identified differential response to biotic stress.

### Transcriptome characterization 
#### 1.Quality control 

Per·review with multiQC
<pre style="color: silver; background: black;">
module load MultiQC/1.1

multiqc .</pre>

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=trimRNA2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=laurafc25@gmail.com
#SBATCH -o trimmRNA2.out
#SBATCH -e trimmRNA2.err
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 TO_R1.fastq TO_R2 fastq output_TO_R1_paired.fq.gz output_TO_R1_unpaired.fq.gz output_TO_R2_paired.fq.gz out_TO_R2_un
paired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 SP_R1.fastq SP_R2.fastq output_SP_R1_paired.fq.gz output_SP_R1_unpaired.fq.gz output_SP_R2_paired.fq.gz out_SP_R2_un
paired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 PA_R1.fastq PA_R2.fastq output_PA_R1_paired.fq.gz output_PA_R1_unpaired.fq.gz output_PA_R2_paired.fq.gz out_PA_R2_un
paired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 NU_R1.fastq NU_R2.fastq output_NU_R1_paired.fq.gz output_NU_R1_unpaired.fq.gz output_NU_R2_paired.fq.gz out_NU_R2_un
paired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 I3_R1.fastq I3_R2.fastq output_I3_R1_paired.fq.gz output_I3_R1_unpaired.fq.gz output_I3_R2_paired.fq.gz out_I3_R2_un
paired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 CHA_R1.fastq CHA_R2.fastq output_CHA_R1_paired.fq.gz output_CHA_R1_unpaired.fq.gz output_CHA_R2_paired.fq.gz output
_CHA_R2_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 MVE_R1.fastq MVE_R2.fastq output_MVE_R1_paired.fq.gz output_MVE_R1_unpaired.fq.gz output_MVE_R2_paired.fq.gz output_
MVE_R2_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 GC_R1.fastq GC_R2.fastq output_GC_R1_paired.fq.gz output_CG_R1_unpaired.fq.gz output_CG_R2_paired.fq.gz output_CG_R2
_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80
java -jar /home/CAM/lcorona/RNA/trimmomatic-0.36.jar PE -phred33 CO_R1.fastq CO_R2.fastq output_CO_R1_paired.fq.gz output_CO_R1_unpaired.fq.gz output_CO_R2_paired.fq.gz output_CO_R2
_unpaired.fq.gz ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:80</pre>

Post·review with multiQC
<pre style="color: silver; background: black;">
module load MultiQC/1.1
/.../output_paired_reads
multiqc .</pre>

#### 2.Asemmbly </pre>
##### Global runs </pre>

Trinity
<pre style="color: silver; background: black;">
module load trinity/2.6.6
Trinity --seqType fq --left R1.fq.gz --right R2.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30</pre>

RNAspades
<pre style="color: silver; background: black;">
module load SPAdes/3.9.0
spades.py -1 R1.fq.gz -2 R2.fq.gz -o RNASpades</pre>

##### Local runs </pre>
Trinity
<pre style="color: silver; background: black;">
module load trinity/2.6.6
Trinity --seqType fq --left output_SP_R1_paired.fq.gz --right output_SP_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_CO_R1_paired.fq.gz --right output_CO_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_PA_R1_paired.fq.gz --right output_PA_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_MVE_R1_paired.fq.gz --right output_MVE_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_NU_R1_paired.fq.gz --right output_NU_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_GC_R1_paired.fq.gz --right output_CG_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
Trinity --seqType fq --left output_TO_R1_paired.fq.gz --right output_TO_R2_paired.fq.gz --min_contig_length 300  --max_memory 200G --CPU 30
Trinity --seqType fq --left output_CHA_R1_paired.fq.gz --right output_CHA_R2_paired.fq.gz --min_contig_length 300  --max_memory 200G --CPU 30
Trinity --seqType fq --left output_I3_R1_paired.fq.gz --right output_I3_R2_paired.fq.gz --min_contig_length 300 --max_memory 200G --CPU 30
</pre>
RNASpades

<pre style="color: silver; background: black;">
module load SPAdes/3.9.0
spades.py -1 output_SP_R1_paired.fq  -2 output_SP_R2_paired.fq  -o RNASpades_SP
spades.py -1 output_CO_R1_paired.fq  -2 output_CO_R2_paired.fq  -o RNASpades_CO
spades.py -1 output_PA_R1_paired.fq  -2 output_PA_R2_paired.fq  -o RNASpades_PA
spades.py -1 output_MVE_R1_paired.fq  -2 output_MVE_R2_paired.fq  -o RNASpades_MV
spades.py -1 output_NU_R1_paired.fq  -2 output_NU_R2_paired.fq  -o RNASpades_NU
spades.py -1 output_GC_R1_paired_fasqc.fq  -2 output_GC_R2_paired_fastqc.fq  -o RNASpades_GC
spades.py -1 output_TO_R1_paired.fq  -2 output_TO_R2_paired.fq  -o RNASpades_TO
spades.py -1 output_CHA_R1_paired.fq  -2 output2_CHA_R2_paired.fq  -o RNASpades_CHA
spades.py -1 output_I3_R1_paired.fq  -2 output_I3_R2_paired.fq  -o RNASpades_I3
</pre>

#### 3.Quality check for assemblers and BUSCO stats for completness

rnaQUAST


BUSCO
<pre style="color: silver; background: black;">
module load busco/4.0.2
module unload augustus
module load python/3.8.1
module load hmmer/3.2.1
module load blast/2.7.1
module load R/3.6.3

export PATH=/home/CAM/lcorona/augustus/bin:/home/CAM/lcorona/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config
busco -i SiPa_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o SP_ensa -m transcriptome
busco -i CON_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o CON_ensa -m transcriptome
busco -i Pal_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o Pal_ensa -m transcriptome
busco -i MVE_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o MVE_ensa -m transcriptome
busco -i NU_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o NU_ensa -m transcriptome
busco -i GC_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o Tol_ensa -m transcriptome
busco -i Tol_en.fasta  -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o Tol_ensa -m transcriptome
busco -i CHA_en.fasta   -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o CHA_ens -m transcriptome
busco -i I3_en.fasta   -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -o I3_ens -m transcriptome
</pre>


#### 4.Clustering by similarity
<pre style="color: silver; background: black;">
module load usearch/9.0.2132
usearch -cluster_fast SiPa_en.fasta -id 0.9 -centroids genes_SP.fasta -uc clusters_SP_genes.uc
usearch -cluster_fast CON_en.fasta -id 0.9 -centroids genes_MAZ.fasta -uc clusters_MAZ_genes.uc
usearch -cluster_fast Pal_en.fasta -id 0.9 -centroids genes_pal.fasta -uc clusters_pal_genes.uc
usearch -cluster_fast MVE_en.fasta -id 0.9 -centroids genes_MV.fasta -uc clusters_MV_genes.uc
usearch -cluster_fast NU_en.fasta -id 0.9 -centroids genes_NU.fasta -uc clusters_NU_genes.uc
usearch -cluster_fast GC_en.fasta -id 0.9 -centroids genes_GC.fasta -uc clusters_GC_genes.uc
usearch -cluster_fast Tol_en.fasta -id 0.9 -centroids genes_tol.fasta -uc clusters_tol_genes.uc
usearch -cluster_fast CHA_en.fasta -id 0.9 -centroids genes_CHA.fasta -uc clusters_CHA_genes.uc
usearch -cluster_fast I3_en.fasta -id 0.9 -centroids genes_I3.fasta -uc clusters_I3_genes.uc
</pre>

The completnees BUSCO analysis was perform  at the end of the analyiss to qualify the processing

#### 5.Identifying Coding Region with TransDecoder 
<pre style="color: silver; background: black;">
module load TransDecoder/5.5.0
TransDecoder.LongOrfs -t genes_SP.fasta
TransDecoder.LongOrfs -t genes_MAZ.fasta
TransDecoder.LongOrfs -t genes_pal.fasta
TransDecoder.LongOrfs -t genes_MV.fasta 
TransDecoder.LongOrfs -t genes_NU.fasta
TransDecoder.LongOrfs -t genes_GC.fasta
TransDecoder.LongOrfs -t genes_tol.fasta
TransDecoder.LongOrfs -t genes_CHA.fasta
TransDecoder.LongOrfs -t genes_I3.fasta
</pre>
The completnees BUSCO analysis was perform  at the end of the analyiss to qualify the processing


#### 6.Global reference reconstruction with Evigene
<pre style="color: silver; background: black;">
hostname
date
module load evigene/20190101 
module load cdhit/4.6.5
module load blast/2.2.29
module load  exonerate/2.4.0
nap=MySppIDPrefix
subd=assemblies_folder


/../evigene/20190101/scripts/rnaseq/trformat.pl -pre TRI -out TRI.tr -log -in genes_TRI.fasta
/../evigene/20190101/scripts/rnaseq/trformat.pl -pre SCA -out SCAP.tr -log -in RNASCAPES.fasta

cat RNASCAPES.fasta gene_TRI.fasta > transcripts.flagstat
/../evigene/scripts/prot/tr2aacds.pl -cdnaseq transcripts.fasta -NCPU 40 -mrnaseq TRI.cds > TRI.tr2aacds
/../evigene/scripts/prot/tr2aacds4.pl -mrna REF.okay.mrna -NCPU=40 
</pre>
The completnees BUSCO analysis was perform  at the end of the analyiss to qualify the processing


### Orthologs annotation and function[](url)
#### 7.Annotation 
<pre style="color: silver; background: black;">
hostname
date
module load EnTAP/0.9.0-beta
module load diamond/0.9.19
module load GeneMarkS-T/5.1   


EnTAP --runN -i REF.okay.okay.okay.cds  -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.87.dmnd  -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.97.dmnd -d /i
sg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd --taxon pinaceae --contam bacteria --contam fungi --contam insecta  --threads 40 --out-dir REF
</pre>

#### 8.Gene family analysis
<pre style="color: silver; background: black;">
module load OrthoFinder/2.4.0
module load muscle
module load DLCpar/1.0
module load FastME
module unload diamond
module load diamond/0.9.36
module load mcl
module load anaconda/4.4.0
orthofinder -f /home/CAM/lcorona/RNA/cleandata/ORTHOFINDER/ -S diamond -t 16 </pre>
</pre>

### Differentially expressed transcripts 

#### 9.Reads counts with Kallisto 
Indexing
<pre style="color: silver; background: black;"> 
echo "kallisto_index"

module load kallisto/0.44.0

kallisto index -i index_REF /home/CAM/lcorona/RNA/cleandata/EVIGENE/publicset/okayset/REF/final_results/annot.fasta 
</pre>

Pseudomapping for location
<pre style="color: silver; background: black;">
module load kallisto/0.44.0

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o CHA_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/Chae/output_CHA_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/Chae/output2_CHA_R2_paired.fq  

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o I3_2 \
        -t 8 -b 100  --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/I3e/output_I3_R1_paired.fq  /home/CAM/lcorona/RNA/cleandata/I3e/output_I3_R2_paired.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o TO_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/output_TO_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/output_TO_R2_paired.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o NU_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/Nune/output_NU_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/Nune/output_NU_R2_paired.fq 

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o MV_2 \
        -t 8 -b 100  --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/Maveen/output_MVE_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/Maveen/output_MVE_R2_paired.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o MZ_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/output_CO_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/output_CO_R2_paired.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF\
        -o GC_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/GCen/output_GC_R1_paired_fasqc.fq /home/CAM/lcorona/RNA/cleandata/GCen/output_GC_R2_paired_fastqc.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o SP_2 \
        -t 8 -b 100  --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/output_SP_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/output_SP_R2_paired.fq

kallisto quant -i /home/CAM/lcorona/RNA/cleandata/Kallisto/index_REF \
        -o PAL_2 \
        -t 8 -b 100 --pseudobam \
        /home/CAM/lcorona/RNA/cleandata/Palmitoe/output_PA_R1_paired.fq /home/CAM/lcorona/RNA/cleandata/Palmitoe/output_PA_R2_paired.fq
</pre>
#### 10.Quality mapping statistics
<pre style="color: silver; background: black;">
module load samtools/1.7

samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/NU/pseudoalignments.bam > NU.txt 
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/CHA/pseudoalignments.bam > CHA.txt 
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/I3/pseudoalignments.bam > I3.txt 
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/TO/pseudoalignments.bam >> TO.txt 
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/MV/pseudoalignments.bam >> MV.txt 
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/SP/pseudoalignments.bam >> SP.txt
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/GC/pseudoalignments.bam >> GC.txt
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/PAL/pseudoalignments.bam >> PAL.txt
samtools flagstat /home/CAM/lcorona/RNA/cleandata/Kallisto/MZ/pseudoalignments.bam >> MZ.txt
</pre>

#### 11.Differential Expression Analysis with DESeq2
<pre style="color: silver; background: black;">
source("http://bioconductor.org/biocLite.R")
biocLite("DESeq2")
biocLite("tximport")
biocLite("readr")
library(tximport)
library(DESeq2)
library(readr)
outputPrefix <- "pinceana"
# Make a vector of all the names of expression data files.
filenames <- list.files(pattern="*.genes.results", full.names=TRUE)
# Peek ;)
head(filenames)
# The the data into R using tximport
txi.rsem <- tximport(filenames, type ="rsem")
# Make "sampleNames" vector
sampleNames <- unlist(strsplit(filenames, "[.]"))
sampleNames

# Make a vector of all the names of expression data files.
# Rename columns of txi.rsem data
sampleNames <- as.vector(window(sampleNames, deltat=4,start =2))
replace(sampleNames, "/", "")
colnames(txi.rsem$counts) <- sampleNames
# Associate condition with the samples
sampleCondition <- c( "N", "C", "N", "C", "S", "S", "N","N", "S")
sampleTable <- data.frame(Condition = sampleCondition, Sample = sampleNames)


# Get data ready to be input to DESeq2
ddstxi <- DESeqDataSetFromTximport(txi.rsem, sampleTable, design = ~ Condition)
# Call the different conditions to be compared
treatments <- c("N","C","S")
colData(ddstxi)$Condition <- factor(colData(ddstxi)$Condition, levels = treatments)
# Calculate differential expression
dds <- DESeq(ddstxi)
res <- results(dds)
transf <- res$padj
transfpad <- -log10(transf)
CFvolData <- data.frame("l2fc" = res$log2FoldChange, "log10padj" = transfpad)
CFvolData <- na.omit(CFvolData)
write.table(CFvolData, file = "CFvolcanodata.csv", sep = ",")
summary(res)
res= subset(res, padj<0.05)
res <- res[order(res$padj),]
resdata <- merge(as.data.frame(res),
                 as.data.frame(counts(dds,normalized =TRUE)),
                 by = 'row.names', sort = FALSE)
names(resdata)[1] <- 'gene'
# Write file with all DEGs with padj below 0.05 
write.csv(resdata, file = paste0(outputPrefix, "-results-with-normalized.csv"))
# send normalized counts to tab delimited file for GSEA, etc.
write.table(as.data.frame(counts(dds),normalized=T), 
            file = paste0(outputPrefix, "_normalized_counts.txt"), sep = '\t')
# Sort into categories based on log2foldchange
pos <- subset(res, log2FoldChange > 2)
neg <- subset(res, log2FoldChange < -2)
upreg2fold <- subset(pos, log2FoldChange < 4)
upreg4fold <- subset(pos, log2FoldChange > 4)
downreg2fold <- subset(neg, log2FoldChange > -4)
downreg4fold <- subset(neg, log2FoldChange < -4)

res2up <- merge(as.data.frame(upreg2fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res2up)[1] <- 'gene'
write.csv(res2up, file = paste0(outputPrefix, "-upreg2fold.csv"))

res4up <- merge(as.data.frame(upreg4fold),
                as.data.frame(counts(dds,normalized =TRUE)),
                by = 'row.names', sort = FALSE)
names(res4up)[1] <- 'gene'
write.csv(res4up, file = paste0(outputPrefix, "-upreg4fold.csv"))

res2down <- merge(as.data.frame(downreg2fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res2down)[1] <- 'gene'
write.csv(res2down, file = paste0(outputPrefix, "-downreg2fold.csv"))

res4down <- merge(as.data.frame(downreg4fold),
                  as.data.frame(counts(dds,normalized =TRUE)),
                  by = 'row.names', sort = FALSE)
names(res4down)[1] <- 'gene'
write.csv(res4down, file = paste0(outputPrefix, "-downreg4fold.csv"))

plotMA(dds, ylim=c(-8,8),main = "RNAseq experiment")
dev.copy(png, paste0(outputPrefix, "-MAplot_initial_analysis.png"))
dev.off()
alpha <- 0.05 # Threshold on the adjusted p-value
cols <- densCols(res$log2FoldChange, -log10(res$pvalue))
plot(res$log2FoldChange, -log10(res$padj), col=cols, panel.first=grid(),
     main="Volcano plot", xlab="Effect size: log2(fold-change)", ylab="-log10(adjusted p-value)",
     pch=20, cex=0.6)
abline(v=0)
abline(v=c(-1,1), col="brown")
abline(h=-log10(alpha), col="brown")

gn.selected <- abs(res$log2FoldChange) > 2.5 & res$padj < alpha 
text(res$log2FoldChange[gn.selected],
     -log10(res$padj)[gn.selected],
     lab=rownames(res)[gn.selected ], cex=0.4)
# transform raw counts into normalized values
# DESeq2 has two options: 1) rlog transformed and 2) variance stabilization
# variance stabilization is very good for heatmaps, etc.
#rld <- rlogTransformation(dds, blind=T)
vsd <- varianceStabilizingTransformation(dds, blind=T)

# save normalized values
write.csv(as.data.frame(assay(rld)),file = paste0(outputPrefix, "-rlog-transformed-counts.txt"))
write.csv(as.data.frame(assay(vsd)),file = paste0(outputPrefix, "-vst-transformed-counts.txt"))
# plot to show effect of transformation
# axis is square root of variance over the mean for all samples
par(mai = ifelse(1:4 <= 2, par('mai'),0))
px <- counts(dds)[,1] / sizeFactors(dds)[1]
ord <- order(px)
ord <- ord[px[ord] < 150]
ord <- ord[seq(1,length(ord),length=50)]
last <- ord[length(ord)]
vstcol <- c('blue','black')
matplot(px[ord], cbind(assay(vsd)[,1], log2(px))[ord, ],type='l', lty = 1, col=vstcol, xlab = 'n', ylab = 'f(n)')
legend('bottomright',legend=c(expression('variance stabilizing transformation'), expression(log[2](n/s[1]))), fill=vstcol)
dev.copy(png,paste0(outputPrefix, "-variance_stabilizing.png"))
dev.off()


ddsClean <- replaceOutliersWithTrimmedMean(dds)
ddsClean <- DESeq(ddsClean)
tab <- table(initial = results(dds)$padj < 0.05,
             cleaned = results(ddsClean)$padj < 0.05)
addmargins(tab)
write.csv(as.data.frame(tab),file = paste0(outputPrefix, "-replaceoutliers.csv"))
resClean <- results(ddsClean)

# filter results by p value
resClean = subset(res, padj<0.05)
resClean <- resClean[order(resClean$padj),]
write.csv(as.data.frame(resClean),file = paste0(outputPrefix, "-replaceoutliers-results.csv"))


</pre>




